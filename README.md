## peertube_embed

A library to embed Peer Tube Videos

### Installation

```
$ gem install peertube_embed
```

### Usage

```ruby
require "peertube_embed"

peertube_embed "https://peertube.xtenz.xyz/videos/watch/f87f381a-8bab-4183-8eb6-a7385ee02d8f"
```
