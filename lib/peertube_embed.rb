def peertube_embed peertube_video_url
  embed_url = peertube_video_url.gsub "/watch/", "/embed/"
  <<-EOS
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="#{embed_url}" frameborder="0" allowfullscreen></iframe>
  EOS
end
