Gem::Specification.new do |s|
  s.name        = 'peertube_embed'
  s.version     = '0.0.0'
  s.date        = '2018-09-28'
  s.summary     = "Provides a function to embed peertube videos"
  s.description = "A simple hello world gem"
  s.authors     = ["Karthikeyan A K"]
  s.email       = 'mindaslab@protonmail.com'
  s.files       = ["lib/peertube_embed.rb"]
  s.homepage    = 'https://gitlab.com/mindaslab/peertube_embed'
  s.license     = 'MIT'
end
